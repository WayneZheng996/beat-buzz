from fastapi.testclient import TestClient
from main import app
from queries.music_queries import MusicQueries


client = TestClient(app)


token = "basjkhdbasjkdbyusafbvasjufvb21312412412"


class MusicQueriesMock:
    def get_new_releases(self, token):
        response = {"topic": {}}
        return response


# Adrian
def test_get_new_releases():
    # Arrange
    app.dependency_overrides[MusicQueries] = MusicQueriesMock
    app.dependency_overrides["token"] = token
    # Act
    response = client.get("/api/browse/new-releases")
    # Assert
    assert response.status_code == 200
    assert response.json() == {"topic": {}}
    # clean up
    app.dependency_overrides = {}
