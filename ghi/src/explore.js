import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function Explore() {
  const [categoryData, setCategoryData] = useState([]);
  const [newsData, setNewsData] = useState([]);
  const [modalShow, setModalShow] = React.useState(false);
  const [modalData, setModalData] = React.useState({});

  useEffect(() => {
    async function getData() {
      const spotifyUrl = `${process.env.REACT_APP_BEATBUZZ_MUSIC_API_HOST}/api/browse/featured-playlists`;
      const spotifyDataResponse = await fetch(spotifyUrl);
      if (spotifyDataResponse.ok) {
        const spotifyData = await spotifyDataResponse.json();
        const data = spotifyData["playlists"]["items"].slice(0, 6); // limits how many post we need
        console.log(data);
        setCategoryData(data);
      }
    }

    async function getNewsData() {
      const newsApiUrl = `${process.env.REACT_APP_BEATBUZZ_MUSIC_API_HOST}/api/news/song`;
      const newsResponse = await fetch(newsApiUrl);
      if (newsResponse.ok) {
        const newsData = await newsResponse.json();
        const data = newsData["articles"].slice(0, 10); // limits how many post we need
        setNewsData(data);
      }
    }
    getData();
    getNewsData();
  }, [setCategoryData, setNewsData]);

  function MyVerticallyCenteredModal(props) {
    const [fetchedData] = useState(modalData);
    const [trackData, setTrackData] = useState([]);

    async function getTrackData() {
      const trackApiUrl = `${process.env.REACT_APP_BEATBUZZ_MUSIC_API_HOST}/api/browse/fetch/?url_link=${modalData.tracks.href}?offset=0&limit=10`;
      const trackResponse = await fetch(trackApiUrl, {
        method: "POST",
      });
      if (trackResponse.ok) {
        const trackData = await trackResponse.json();
        const data = trackData["items"].slice(0, 20);
        console.log(data);
        setTrackData(data);
      }
    }

    if (Object.keys(fetchedData).length > 1) {
      if (trackData.length === 0) {
        getTrackData();
      }
    }

    return (
      <Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">{fetchedData.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3 className="text-center">Playlists</h3>
          <ul className="list-group list-group-flush">
            {trackData.map((album) => {
              return (
                <li className="list-group-item d-flex align-items-center" key={album.track.album.name}>
                  <div className="image">
                    <a href={album.track.album.external_urls.spotify} target="_blank" rel="noopener noreferrer">
                      <img className="img" src={album.track.album.images[0].url} alt={album.track.album.name} style={{ height: "4rem", width: "8rem" }} />
                    </a>
                  </div>
                  <div style={{ marginLeft: "100px", marginRight: "100px" }}>
                    <a href={album.track.album.external_urls.spotify} style={{ textDecoration: "none", color: "#000000" }} target="_blank" rel="noopener noreferrer">
                      <b>{album.track.album.name} </b> &nbsp; &nbsp; -By. {album.track.album.artists[0].name}
                    </a>
                  </div>
                </li>
              );
            })}
          </ul>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  function characterCheck(character_string) {
    console.log(character_string);
    console.log(character_string.length);
    if (character_string.length < 30) {
      return character_string + " Click button below to show playlist";
    } else if (character_string.length > 70) {
      return character_string.slice(0, 70);
    } else {
      return character_string;
    }
  }

  return (
    <div>
      <div className="container text-center">
        <h2 className="text-center">Explorations</h2>
        <div className="row align-items-start">
          {categoryData.map((album) => {
            return (
              <div className="col mb-4" key={album.id}>
                <div className="card" style={{ height: "fit-content", width: "21rem" }}>
                  <img src={album.images[0].url} className="card-img-top" alt={album.description} style={{ height: "19.5rem", width: "21rem" }} />
                  <div className="card-body">
                    <b>{album.name.trim()}</b>
                    <p className="card-text">{characterCheck(album.description.split("Cover")[0])}</p>
                    <Button variant="primary" onClick={() => [setModalData(album), setModalShow(true)]}>
                      Show Playlist
                    </Button>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <MyVerticallyCenteredModal show={modalShow} onHide={() => [setModalData({}), setModalShow(false)]} />
      <div className="container text-center my-4 py-4">
        <h2 className="text-center mb-4">News</h2>
        <ul className="list-group mb-4">
          {newsData.map((article) => {
            return (
              <li className="list-group-item d-flex align-items-center" key={article.url}>
                <div className="image">
                  <a href={article.url} target="_blank" rel="noopener noreferrer">
                    <img className="img" src={article.urlToImage} alt={article.urlToImage} style={{ height: "10rem", width: "20rem" }} />
                  </a>
                </div>
                <div style={{ marginLeft: "100px", marginRight: "100px" }}>
                  <a href={article.url} style={{ textDecoration: "none", color: "#000000" }} target="_blank" rel="noopener noreferrer">
                    {article.description.trim()}
                  </a>
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}

export default Explore;
