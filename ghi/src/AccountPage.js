import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';
import './App.css';
import { useAuthContext } from "./UseToken";
import { useToken } from "./UseToken";



const AccountPage = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [user, setUser] = useState({});


  useEffect(() => {
    if (localStorage.getItem("token")) {
      setIsLoggedIn(true);
    }
  }, []);

  const handleLogin = (event) => {
    event.preventDefault();
    const email = event.target.elements.email.value;
    const password = event.target.elements.password.value;
    if (email === 'user@example.com' && password === 'password') {
      setIsLoggedIn(true);
      setUser({ email: 'user@example.com', name: 'User' });
      localStorage.setItem("token", "loggedIn"); // save token to localStorage
    } else {
      alert('Incorrect email or password');
    }

  };

  function HandleLoginCheck() {
    const { token } = useAuthContext();
    const protectUrl = `${process.env.REACT_APP_BEATBUZZ_API_HOST}/token`;
      const response = fetch(protectUrl, {
        credentials: 'same-origin'
      }).then((response) => response.json().then((data) =>{
        console.log(useToken);
      }))
    }

  HandleLoginCheck();

  return isLoggedIn ? (
    <Container>
      <Row>
        <Col>
          <Card>
            <Card.Header>Profile</Card.Header>
            <Card.Body>
              <Card.Title>Welcome, {user.name}</Card.Title>
              <Card.Text>Email: {user.email}</Card.Text>
              <Button variant="primary">Edit Profile</Button>
              <Button variant="danger" className="ml-3">Delete Account</Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  ) : (
    <Container>
      <Row>
        <Col>
        <div className="to-view-container">
          <h2>Please log in to view your profile.</h2></div>
          <Form onSubmit={handleLogin}>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" placeholder="Enter email" name="email"/>
            </Form.Group>
            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" name="password" />
            </Form.Group>
            <Button variant="primary" type="submit">
              Log In
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
  }

  export default AccountPage;
