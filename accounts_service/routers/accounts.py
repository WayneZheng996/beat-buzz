from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from models import AccountForm, AccountToken, HttpError
from .auth import authenticator
from pydantic import BaseModel
from queries.accounts_queries import (
    Account,
    AccountIn,
    AccountOut,
    AccountQueries,
    AccountUpdateIn,
    DuplicateAccountError,
)


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


# define if user are logged in
@router.get("/api/protected", response_model=bool)
async def get_protected(
    account_data: dict = Depends(authenticator.try_get_current_account_data),
):
    if account_data:
        return True
    else:
        return False


@router.post("/api/accounts/", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = repo.create(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(username=info.email, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.get("/api/accounts/{account_email}", response_model=AccountOut)
async def get_account(
    account_email: str,
    response: Response,
    repo: AccountQueries = Depends(),
):
    record = repo.get(account_email)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.put(
    "/api/accounts/{account_id}", response_model=AccountUpdateIn | HttpError
)
async def update_account(
    account_id: str,
    info: AccountUpdateIn,
    request: Request,
    response: Response,
    repo: AccountQueries = Depends(),
):
    if not info.password:
        hashed_password = authenticator.hash_password(info.password)
    else:
        hashed_password = None

    try:
        account = repo.update(account_id, info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot Update An Account With Those Credentials",
        )
    return repo.get_by_id(account_id)


@router.get("/api/accounts/", response_model=list[AccountOut])
async def get_accounts(repo: AccountQueries = Depends()):
    return repo.get_all()


@router.delete("/api/accounts/{account_id}", response_model=bool)
async def delete_accounts(account_id: str, repo: AccountQueries = Depends()):
    return True if repo.delete(id=account_id) else False
