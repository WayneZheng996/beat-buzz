## 12/20/2022

- Helped collaborating and writing on excalidraw

## 12/21/22

- Worked on wireframing and talked about api's with teammates
- written down api idea and enpoints will be used

## 12/22/22

- Updated wireframing per instructor's recommendations
- signed up api end points credentials
- doing 3rd party api end point tests

## 1/3/2023

- organized and added few additional feature to wire framing, including
- related api's that will be utilized, visualized the connection between each
- functioncality and how would one page operates and refactor into another page

## 1/4/2023

- researching on MongoDB and picturing the whole roadmap of how project will be written
- building fundamental blocks and knowledge on fast api with mongoDB before taking further actions

## 1/9/2023

- ensure api usability and stability within the permitted api calls per minutes
- working on backend service, implementing 3rd party api calls

## 1/10/2023

- continue working on backend api, getting CRUD functionality finished and implement api usage
- confirmed that thottleing limit on spotify api and news api

## 1/11/2023

- changed whole backend structure, re-assign database dedicated to user account service only
- implemented 3rd party api with time expiration
- added api token file to gitignore file to avoid sensetive data push

## 1/12/2023

- re-factor user authentication endpoints and models
- ensure clean and secured flow between database and authentication endpoint
- re-implementing mongoDB to accounts-service module instead of music-service

## 1/13/2023

- debugging authentications - blocker

## 1/17/2023

- implemented fully functional CRUD authentication
- tweaked 3rd party api to support embedded requests
- pre-written session

## 1/18/2023

- working on frontend page of categories
- implementing backend api connection to frontend page rendering
- debugging peers env and docker

## 1/19/2023

- continue working on categories frontpage with react
- debugging peers api's interaction issues
- debugging peers code, that they left behind

## 1/20/2023

- finishing up categories page
- start working on CI/CD implementation
- start working on unit test

## 1/21/2023

- added headline news api
- implemented explorings page with categories page
- implemented news to explorations page

## 1/23/2023

- uploaded frontend page explore
- backed up github issue
- implementing CI/CD

## 1/24/2023

- Finished CI/CD changed apikeys from py frome to .env
- adding more functionality to explore page

## 1/25/2023

- finished explore page with full functionality
- working on re write user base with depends from fast api

## 1/26/2023

- finished writing api documentations
- implementing CI/CD
- writing unit tests
