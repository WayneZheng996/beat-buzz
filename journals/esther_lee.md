## January 27, 2023
Finished account create unit test.
Updated ReadMe file.

## January 26, 2023
Trying to get token to store into local storage.
Working on unit testing.
Uploaded wire frames.

## January 25, 2023
Went through project rubric to make sure our project meets the requirements.
    Installed black.
Pulled down changes teammates merged onto main, found:
    Warning: Invalid DOM property `class`.
Tried to help Adrian with an error for search bar:
    Uncaught TypeError: Cannot read properties of undefined (reading 0)

## January 24, 2023
Tried to figure out merge conflicts between teammates. For some reason individual pushes
were also being pushed into other people's branches.
Worked with Jerrika on trying to figure out why her branch continued to have a rebase error.
Started on ReadMe.

## January 23, 2023
Tried to help Jerrika with merge conflicts.
Working on trying to get the logout button to only show up if user is logged in.

## January 20, 2023
Fixed login/signup buttons to redirect to correct place.
Created logout.
Fixed authorization, signup, login, and logout now works.

## January 19, 2023
kept getting 422 error for login and sign up. fixed bug after correcting json stringify function.

## January 18, 2023
Succesfully got the token to work in account form. Now implementing token into login form.
Succesfully got the account form to navigate to home after created.

## January 17, 2023
Worked on trying to get token to work on sign up and login.

## January 13, 2023
Worked with Jerrika trying to get webpage theme to work.

## January 12, 2023
Worked on Mainpage, got a logo to show up and fixed the nav bar.
Trying to figure out how to

## January 10, 2023
Continued working on Nav.js trying to figure out bug.
Asked instructor for help and found that a dependency in package.json
was improperly installed.
After reintalling correct dependencies, Nav.js successfully runs.
Succesfully connected Account.js.

## January 9, 2023
Successfully created a working main page.
Worked with Jerrika trying to figure out Nav.js bug.
    Error message: Module not found: Error: Can't resolve 'react-router-dom' in '/app/src'


## January 8, 2023
Created forms directory for React.
Created AccountForm.js
Attempted to use hooks to retrieve user input using useState.


## January 6, 2023
Worked with Jerrika.
Added files to queries and routers directories.
Still trying to figure out how MongoDB is working.

## January 4, 2023
Worked on project setup
    - Got a bug after creating mongoclient.
    - Worked on trying to fix bug with Jerrika.
    - GHI container was not running anymore. However, nobody ever went into the GHI directory.
        Kept getting Error: Cannot find module '/app/node_modules/inherits/inherits.js'. Please verify that the package.json has a valid "main" entry.
    - Bug fixed after reinstalling npm and rebuilding
    - Pushed all working code.

## January 3, 2023
Oraganized wireframing and made prettier to look at.
Forked project gamma, added team members, and created own branch.
Started the project setup following D1:project advice.
    - Completed Supporting Windows and non-Windows
    - Added mongoDB to docker-compose.yaml
    - Created requirements.txt
    - Pushed all working code

## December 22, 2022
Updated wireframing per instructor's recommendations.

## December 21, 2022
Worked on wireframing and talked about api's with teammates.

## December 20, 2022
Created excalidraw file and started wireframing ideas on excalidraw.
    - created pages with teammates.
